$(function() {

	// solving the active menu
	switch (menu) {
	case 'Home':
		$('#home').addClass('active');
		break;
	case 'My account':
		$('#myaccount').addClass('active');
		break;
	case 'My order':
		$('#myorder').addClass('active');
		break;
	case 'Manage Products':
		$('#manageProducts').addClass('active');
		break;
	case 'All Products':
		$('#listProducts').addClass('active');
		break;
	case 'Login':
		$('#login').addClass('active');
		break;
	case 'Register':
		$('#register').addClass('active');
		break;
	default:
		$('#c_' + menu).addClass('active');
		break;
	}

	// dismissing the alert after 3s
	$alert = $('.alert');
	if ($alert.length) {
		setTimeout(function() {
			$alert.fadeOut('slow');
		}, 3000);
	}

	// product detail page add button click
	$('#add_price').click(
			function() {
				$('#bidding_price').val(
						parseFloat($('#bidding_price').val())
								+ parseFloat(addAmount));
			});

	$('#minus_price').click(
			function() {
				if (parseFloat($('#bidding_price').val()) > currentPrice) {
					$('#bidding_price').val(
							parseFloat($('#bidding_price').val())
									- parseFloat(addAmount));
				}
			});

	// $.getJSON(
	// url="/sydbay/account/json/data/user/1",
	// function (data) {
	// //iterate accessing data
	//				
	// $("#myAcc").append("<p>User Name: " + data.username + "</p>"
	// + "<p>User Email: " + data.email + "</p>"
	// + "<p>User Password: " + data.password + "</p>"
	//								 
	// + "<p>User Credit: " + data.credit + "</p>");
	// }
	// );

	// $('#myAcc').html("xxxxxxx");
	$('#history').DataTable();

	// register validation
	/*$loginForm = $('#register-form');

	if ($loginForm.length) {

		$loginForm.validate({
			rules : {
				confirmPassword : {
					required : true,
					email : true

				}
			},
			messages : {
				username : {
					required : 'Please enter your email!',
					email : 'Please enter a valid email address!'
				},
				password : {
					required : 'Please enter your password!'
				}
			},
			errorElement : "em",
			errorPlacement : function(error, element) {
				// Add the 'help-block' class to the error element
				error.addClass("help-block");

				// add the error label after the input element
				error.insertAfter(element);
			}
		}

		);

	}*/

	$('.switch input[type=checkbox]')
			.on(
					'change',
					function() {
						var checkbox = $(this);
						var checked = checkbox.prop('checked');
						var value = checkbox.prop('value');
						var dMsg = (checked) ? 'You want to activate the Product?'
								: 'You want to de-activate the Product?';

						bootbox
								.confirm({
									size : 'medium',
									title : 'Products Activition and Deactivation',
									message : dMsg,
									callback : function(confirmed) {
										if (confirmed) {
											console.log(value);
											bootbox
													.alert({
														size : 'medium',
														title : 'Information',
														message : 'You are going to perform operation on product '
																+ value
													});
										} else {
											checkbox.prop('checked', !checked);
										}
									}
								});
					});

	// view product page list table
	var $table = $('#productListTable');

	if ($table.length) {
		// console.log('Inside the table');

		var jsonUrl = '';
		if (window.categoryId == '') {
			jsonUrl = window.contextRoot + '/json/data/all/products';
		} else {
			jsonUrl = window.contextRoot + '/json/data/category/'
					+ window.categoryId + '/products';
		}

		$table
				.DataTable({
					lengthMenu : [ [ 3, 5, 10, -1 ],
							[ '3 Records', '5 Records', '10 Records', 'ALL' ] ],
					pageLength : 5,
					ajax : {
						url : jsonUrl,
						dataSrc : ''
					},
					columns : [
							{
								data : 'code',
								bSortable : false,
								mRender : function(data, type, row) {

									return '<img src="' + window.contextRoot
											+ '/resources/images/' + data
											+ '.jpg" class="dataTableImg"/>';

								}
							},
							{
								data : 'name'
							},
							{
								data : 'duration',
								mRender : function(data, type, row) {
									return data + ' days'
								}
							},
							{
								data : 'initPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'addAmount',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'fixedPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'currentPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'id',
								bSortable : false,
								mRender : function(data, type, row) {
									var str = ''
									str += '<a href="'
											+ window.contextRoot
											+ '/show/'
											+ data
											+ '/product" class="btn btn-warning"><span class="fa fa-eye"></span></a> &#160;';
									return str;
								}
							} ]
				});
	}

	// list of all products for admin
	var $adminProductsTable = $('#adminProductsList');

	if ($adminProductsTable.length) {
		console.log('Inside the table');
		var jsonUrl = window.contextRoot + '/json/data/admin/all/products';
		console.log(jsonUrl);

		$adminProductsTable
				.DataTable({
					lengthMenu : [ [ 10, 30, 50, -1 ],
							[ '10 Records', '30 Records', '50 Records', 'ALL' ] ],
					pageLength : 30,
					ajax : {
						url : jsonUrl,
						dataSrc : ''
					},
					columns : [
							{
								data : 'id'
							},

							{
								data : 'code',
								bSortable : false,
								mRender : function(data, type, row) {
									return '<img src="'
											+ window.contextRoot
											+ '/resources/images/'
											+ data
											+ '.jpg" class="adminDataTableImg"/>';
								}
							},
							{
								data : 'name'
							},
							{
								data : 'duration',
								mRender : function(data, type, row) {
									return data + ' days'
								}
							},
							{
								data : 'initPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'addAmount',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'fixedPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'currentPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'active',
								bSortable : false,
								mRender : function(data, type, row) {
									var str = '';
									if (data) {
										str += '<label class="switch"> <input type="checkbox" value="'
												+ row.id
												+ '" checked="checked">  <div class="slider round"> </div></label>';

									} else {
										str += '<label class="switch"> <input type="checkbox" value="'
												+ row.id
												+ '">  <div class="slider round"> </div></label>';
									}

									return str;
								}
							},
							{
								data : 'id',
								bSortable : false,
								mRender : function(data, type, row) {

									var str = '';
									str += '<a href="'
											+ window.contextRoot
											+ '/manage/'
											+ data
											+ '/product" class="btn btn-primary"><span class="fa fa-pencil"></span></a> &#160;';

									return str;
								}
							} ],

					initComplete : function() {
						var api = this.api();
						api
								.$('.switch input[type=checkbox]')
								.on(
										'change',
										function() {
											var checkbox = $(this);
											var checked = checkbox
													.prop('checked');
											var value = checkbox.prop('value');
											var dMsg = (checked) ? 'You want to activate the Product?'
													: 'You want to de-activate the Product?';

											bootbox
													.confirm({
														size : 'medium',
														title : 'Products Activition and Deactivation',
														message : dMsg,
														callback : function(
																confirmed) {
															if (confirmed) {
																console
																		.log(value);
																var activationUrl = window.contextRoot
																		+ '/manage/product/'
																		+ value
																		+ '/activation';

																$
																		.post(
																				activationUrl,
																				function(
																						data) {
																					bootbox
																							.alert({
																								size : 'medium',
																								title : 'Information',
																								message : data
																							});
																				});

															} else {
																checkbox
																		.prop(
																				'checked',
																				!checked);
															}
														}
													});
										});

					}

				});
	}

	// user order
	var $table = $('#userOrderTable');

	if ($table.length) {
		// console.log('Inside the table');

		var jsonUrl = window.contextRoot + '/json/data/user/' + uid + '/orders';

		$table
				.DataTable({
					lengthMenu : [ [ 3, 5, 10, -1 ],
							[ '3 Records', '5 Records', '10 Records', 'ALL' ] ],
					pageLength : 5,
					ajax : {
						url : jsonUrl,
						dataSrc : ''
					},
					columns : [
							{
								data : 'code',
								bSortable : false,
								mRender : function(data, type, row) {

									return '<img src="' + window.contextRoot
											+ '/resources/images/' + data
											+ '.jpg" class="dataTableImg"/>';

								}
							},
							{
								data : 'name'
							},
							{
								data : 'duration',
								mRender : function(data, type, row) {
									return data + ' days'
								}
							},
							{
								data : 'initPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'addAmount',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'fixedPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'currentPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'id',
								bSortable : false,
								mRender : function(data, type, row) {
									var str = ''
									str += '<a href="'
											+ window.contextRoot
											+ '/show/'
											+ data
											+ '/product" class="btn btn-info"><span class="fa fa-ticket"></span></a> &#160;';
									return str;
								}
							} ]
				});
	}

	// list all users for admin
	var $adminProductsTable = $('#adminUserList');

	if ($adminProductsTable.length) {
		console.log('Inside the table');
		var jsonUrl = window.contextRoot + '/json/data/admin/users';
		console.log(jsonUrl);

		$adminProductsTable
				.DataTable({
					lengthMenu : [ [ 10, 30, 50, -1 ],
							[ '10 Records', '30 Records', '50 Records', 'ALL' ] ],
					pageLength : 30,
					ajax : {
						url : jsonUrl,
						dataSrc : ''
					},
					columns : [
							{
								data : 'id'
							},

							{
								data : 'code',
								bSortable : false,
								mRender : function(data, type, row) {
									return '<img src="'
											+ window.contextRoot
											+ '/resources/images/'
											+ data
											+ '.jpg" class="adminDataTableImg"/>';
								}
							},
							{
								data : 'name'
							},
							{
								data : 'duration',
								mRender : function(data, type, row) {
									return data + ' days'
								}
							},
							{
								data : 'initPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'addAmount',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'fixedPrice',
								mRender : function(data, type, row) {
									return '&#36; ' + data
								}
							},
							{
								data : 'active',
								bSortable : false,
								mRender : function(data, type, row) {
									var str = '';
									if (data) {
										str += '<label class="switch"> <input type="checkbox" value="'
												+ row.id
												+ '" checked="checked">  <div class="slider round"> </div></label>';

									} else {
										str += '<label class="switch"> <input type="checkbox" value="'
												+ row.id
												+ '">  <div class="slider round"> </div></label>';
									}

									return str;
								}
							},
							{
								data : 'id',
								bSortable : false,
								mRender : function(data, type, row) {

									var str = '';
									str += '<a href="'
											+ window.contextRoot
											+ '/manage/'
											+ data
											+ '/product" class="btn btn-primary"><span class="fa fa-pencil"></span></a> &#160;';

									return str;
								}
							} ],

					initComplete : function() {
						var api = this.api();
						api
								.$('.switch input[type=checkbox]')
								.on(
										'change',
										function() {
											var checkbox = $(this);
											var checked = checkbox
													.prop('checked');
											var value = checkbox.prop('value');
											var dMsg = (checked) ? 'You want to activate the Product?'
													: 'You want to de-activate the Product?';

											bootbox
													.confirm({
														size : 'medium',
														title : 'Products Activition and Deactivation',
														message : dMsg,
														callback : function(
																confirmed) {
															if (confirmed) {
																console
																		.log(value);
																var activationUrl = window.contextRoot
																		+ '/manage/product/'
																		+ value
																		+ '/activation';

																$
																		.post(
																				activationUrl,
																				function(
																						data) {
																					bootbox
																							.alert({
																								size : 'medium',
																								title : 'Information',
																								message : data
																							});
																				});

															} else {
																checkbox
																		.prop(
																				'checked',
																				!checked);
															}
														}
													});
										});

					}

				});
	}

});
