<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container">
	<div class="row">
		<%@include file="./shared/sidebar.jsp"%>
		<div class="col-lg-10">

			<!-- product list based on category user clicked -->
			<div class="row">
				<div class="col-lg-12">
					<c:if test="${userClickAllProducts == true}">
						<script>
							window.categoryId = '';
						</script>

						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="${contextRoot}/home">Home</a></li>
							<li class="breadcrumb-item active">All Products</li>
						</ol>



					</c:if>

					<c:if test="${userClickCategory == true}">
						<script>
							window.categoryId = '${category.id}';
						</script>

						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="${contextRoot }/home">Home</a></li>
							<li class="breadcrumb-item active">Category</li>
							<li class="breadcrumb-item active">${category.name }</li>
						</ol>
					</c:if>
					
											<div class="row">
							<div class="col-12">
								<table id="productListTable"
									class="table table-striped table-borderd">
									<thead>
										<tr>
											<th></th>
											<th>Name</th>
											<th>Duration</th>
											<th>Initial Price</th>
											<th>Add Amount</th>
											<th>Buy-it-now Price</th>
											<th>Current Price</th>
											<th></th>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<th></th>
											<th>Name</th>
											<th>Duration</th>
											<th>Initial Price</th>
											<th>Add Amount</th>
											<th>Buy-it-now Price</th>
											<th>Current Price</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					
				</div>
			</div>

		</div>
	</div>
</div>