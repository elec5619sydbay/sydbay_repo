<%@include file="./flows/shared/login-navbar.jsp"%>

<div>
	<p></p>
	<h1 class="text-center">Welcome!</h1>
	<p></p>
</div>
<!-- content -->
<div class="container">
	<div class="row">
		<c:if test="${not empty message }">

			<div class="col-12 text-center">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&#xD7;</button>${message }</div>
			</div>
		</c:if>

		<div class="col-lg-6 mx-md-auto">
			<div class="card text-center">
				<div class="card-header">
					<ul class="nav nav-tabs card-header-tabs">
						<li class="nav-item col-12"><a class="nav-link active"
							href="#" id="login-form-link">Login</a></li>
					</ul>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-xl-12">
							<sf:form id="login-form" action="${contextRoot }/signin"
								method="POST" modelAttribute="user">
								<div class="form-group">
									<sf:input type="text" path="username" id="username"
										tabindex="1" class="form-control" placeholder="Username"
										value="" />
									<sf:errors path="username" cssClass="help-block" element="em"></sf:errors>
								</div>
								<div class="form-group">
									<sf:input type="password" path="password" id="password"
										tabindex="2" class="form-control" placeholder="Password" />
									<sf:errors path="password" cssClass="help-block" element="em"></sf:errors>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6 mx-sm-auto">
											<input type="submit" name="login-submit" id="login-submit"
												tabindex="4" class="form-control btn btn-login"
												value="Log In">
											<sf:hidden path="id" />
											<sf:hidden path="credit" />
											<sf:hidden path="avatar" />
											<sf:hidden path="email" />
											<sf:hidden path="confirmPassword" />
										</div>
									</div>
								</div>
							</sf:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="./flows/shared/login-footer.jsp"%>