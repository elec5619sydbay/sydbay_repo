<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container">

	<div class="row">
		<c:if test="${not empty message}">
			<div class="col-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					${message}
				</div>
			</div>
		</c:if>
		<div class="mx-md-auto col-lg-8">
			<div class="card">
				<div class="card-header text-white bg-primary">
					<h4>Product Management</h4>

				</div>

				<div class="panel-body">

					<!-- FORM START HERE -->
					<sf:form modelAttribute="product"
						action="${contextRoot}/manage/products" method="POST"
						enctype="multipart/form-data">
						<div class="form-group">
							<label class="form-control-label col-md-4" for="name">
								Title: </label>
							<div class="col-md-8">
								<sf:input type="text" path="name" id="name"
									placeholder="Product Name" class="form-control" />
								<sf:errors path="name" class="form-text text-danger"
									element="em"></sf:errors>
							</div>
						</div>

						<!-- File update (image) -->
						<div class="form-group">
							<label class="form-control-label col-md-4" for="file">
								Select an Image: </label>
							<div class="col-md-8">
								<sf:input type="file" path="file" id="file" class="form-control" />
								<sf:errors path="file" class="form-text text-danger"
									element="em"></sf:errors>
							</div>
						</div>

						<div class="form-group">
							<label class="form-control-label col-md-4" for="duration">
								Duration: </label>
							<div class="col-md-8">
								<sf:input type="number" path="duration" id="duration"
									placeholder="Duration" class="form-control" />
								<sf:errors path="duration" class="form-text text-danger"
									element="em" />
							</div>
						</div>

						<div class="form-group">
							<label class="form-control-label col-md-4" for="initPrice">
								Initial Price: </label>
							<div class="col-md-8">
								<sf:input type="number" path="initPrice" id="initPrice"
									placeholder="Initial Price" class="form-control" />
								<sf:errors path="initPrice" class="form-text text-danger"
									element="em" />
							</div>
						</div>

						<div class="form-group">
							<label class="form-control-label col-md-4" for="addAmount">
								Add Amount: </label>
							<div class="col-md-8">
								<sf:input type="number" path="addAmount" id="addAmount"
									placeholder="Add Amount" class="form-control" />
								<sf:errors path="addAmount" class="form-text text-danger"
									element="em" />
							</div>
						</div>

						<div class="form-group">
							<label class="form-control-label col-md-4" for="fixedPrice">
								Fixed Price: </label>
							<div class="col-md-8">
								<sf:input type="number" path="fixedPrice" id="fixedPrice"
									placeholder="Fixed Price" class="form-control" />
								<sf:errors path="fixedPrice" class="form-text text-danger"
									element="em" />
							</div>
						</div>

						<div class="form-group">
							<label class="form-control-label col-md-4" for="productCondition">
								Product Condition: </label>
							<div class="col-md-8">
								<sf:select path="productCondition"
									class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control"
									id="inlineFormCustomSelect">
									<option value="-1" selected>Choose...</option>
									<option value="0">Brand New</option>
									<option value="1">Used</option>
									<option value="2">Old</option>
								</sf:select>
								<sf:errors path="productCondition" class="form-text text-danger"
									element="em" />
							</div>
						</div>

						<div class="form-group">
							<label class="form-control-label col-md-4" for="description">
								Description: </label>
							<div class="col-md-8">
								<sf:textarea path="description" id="description"
									placeholder="Product Description" class="form-control" />
								<sf:errors path="description" class="form-text text-danger"
									element="em" />
							</div>
						</div>

						<div class="form-group">
							<label class="form-control-label col-md-4" for="categoryId">
								Select Category: </label>

							<div class="col-md-8">
								<sf:select
									class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control"
									id="categoryId" path="categoryId" items="${categories}"
									itemLabel="name" itemValue="id" />
								<c:if test="${product.id == 0 }">
									<br />
									<button id="btn-ctg" type="button"
										class="btn btn-warning btn-xs" data-toggle="modal"
										data-target="#myCategoryModal">Add New Category</button>
								</c:if>
							</div>
						</div>

						<div class="form-group">
							<label for="example-date-input" class="col-2 col-form-label">Begin
								at: </label>
							<div class="col-10">
								<sf:input path="startDate" class="form-control" type="date"
									value="2017-10-19" id="example-date-input" />
								<sf:errors path="startDate" class="form-text text-danger"
									element="em" />
							</div>
						</div>


						<div class="form-group">
							<div class="mx-4-auto col-md-8">
								<input type="submit" name="submit" id="submit" value="Submit"
									class="btn btn-primary" />
								<!-- Hidden fields -->
								<sf:hidden path="id" />
								<sf:hidden path="code" />
								<sf:hidden path="supplierId" />
								<sf:hidden path="purchases" />
								<sf:hidden path="views" />
								<sf:hidden path="status" />
								<sf:hidden path="active" />
							</div>
						</div>
					</sf:form>
				</div>
			</div>

		</div>

	</div>

	<!-- Product table for Admin -->
<!-- 	<div class="row">
		<div class="col-12">
			<h3>Available Products</h3>
			<hr />
		</div>
		<div class="col-12">
			<table id="adminProductsList" class="table table-sm table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Image</th>
						<th>Name</th>
						<th>duration</th>
						<th>Initial Price</th>
						<th>Add Amount</th>
						<th>Fixed Price</th>
						<th>Active</th>
						<th>Edit</th>
					</tr>
				</thead>


				<tfoot>
					<tr>
						<th>Id</th>
						<th>Image</th>
						<th>Name</th>
						<th>duration</th>
						<th>Initial Price</th>
						<th>Add Amount</th>
						<th>Fixed Price</th>
						<th>Active</th>
						<th>Edit</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
 -->

	<!-- Modal -->
	<div class="modal fade" id="myCategoryModal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span>&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Add New Category</h4>
				</div>

				<div class="modal-body">

					<sf:form id="categoryForm" class="form-horizontal"
						modelAttribute="category" action="${contextRoot}/manage/category"
						method="POST">

						<div class="form-group">
							<label class="control-label col-md-4">Name</label>
							<div class="col-md-8 validate">
								<sf:input type="text" path="name" class="form-control"
									placeholder="Category Name" />
							</div>
						</div>

						<div class="form-group">
							<div class="mx-md-auto col-lg-4">
								<input type="submit" name="submit" value="Save"
									class="btn btn-primary">
							</div>
						</div>
					</sf:form>
				</div>
			</div>
		</div>
	</div>

</div>
