<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextRoot" value="${pageContext.request.contextPath }" />

<spring:url var="css" value="/resources/css"></spring:url>
<spring:url var="js" value="/resources/js"></spring:url>
<spring:url var="images" value="/resources/images"></spring:url>


<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">


<title>SYDBay - ${title }</title>

<script>
	window.menu = '${title }';
	window.contextRoot = '${contextRoot}';
	window.uid = '${user.id }';
</script>

<!-- Bootstrap core CSS -->
<link href="${css }/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="${css }/page.css" rel="stylesheet">

<%-- <link href="${css }/dataTable.bootstrap.min.css" rel="stylesheet"> --%>

<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.css" />

</head>

<body>


	<%@include file="./shared/navbar.jsp"%>

	<!-- Page Content -->
	<!-- Loading home page here -->
	<c:if test="${userClickHome == true }">
		<div class="content">

			<%@include file="home.jsp"%>

		</div>
	</c:if>

	<!-- Loading my account page here -->
	<c:if test="${userClickAcc == true }">
		<div class="content">

			<%@include file="myaccount.jsp"%>

		</div>
	</c:if>

	<!-- Loading my order page here -->
	<c:if test="${userClickOrder == true }">
		<div class="content">

			<%@include file="myorder.jsp"%>

		</div>
	</c:if>

	<!-- Loading if user click category name -->
	<c:if test="${userClickCategory == true }">
		<div class="content">

			<%@include file="listProduct.jsp"%>

		</div>
	</c:if>

	<!-- Loading if user click single product -->
	<c:if test="${userClickShowProduct == true }">
		<div class="content">

			<%@include file="singleProduct.jsp"%>

		</div>
	</c:if>

	<!-- Loading if user click manage product -->
	<c:if test="${userClickManageProducts == true }">
		<div class="content">

			<%@include file="manageProducts.jsp"%>

		</div>
	</c:if>

	<!-- Loading if user click view products -->
	<c:if test="${userClickAllProducts == true }">
		<div class="content">

			<%@include file="listProduct.jsp"%>

		</div>
	</c:if>

	<!-- Loading if user click view products -->
	<c:if test="${userClickAdminHome == true }">
		<div class="content">

			<%@include file="adminhome.jsp"%>

		</div>
	</c:if>

	<!-- Footer -->
	<%@include file="./shared/footer.jsp"%>


	<!-- Bootstrap core JavaScript -->
	<script src="${js }/jquery.min.js"></script>
	<script src="${js }/jquery.validate.js"></script>
	<script src="${js }/popper.min.js"></script>
	<script src="${js }/bootstrap.min.js"></script>
	<script src="${js }/bootstrap.js"></script>
	<script src="${js }/countdown.js" type="text/javascript"></script>
	<script src="${js }/bootbox.min.js"></script>
	<script type="text/javascript"
		src="https://cdn.datatables.net/v/bs4/dt-1.10.16/datatables.min.js"></script>
	<script src="${js }/myapp.js"></script>

	</div>
</body>

</html>