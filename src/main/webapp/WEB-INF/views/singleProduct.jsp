<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container">

	<!-- display breadcrumb -->
	<div class="row">
		<div class="col-xs-12 col-lg-12">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="${contextRoot }/home">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Products</a></li>
				<li class="breadcrumb-item active">${product.name }</li>
			</ol>
		</div>
	</div>

	<c:if test="${not empty message }">

		<div class="col-12 text-center">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&#xD7;</button>${message }</div>
		</div>
	</c:if>
	<div class="row">

		<!-- display the product image -->
		<div class="col-xs-12 col-sm-1"></div>
		<div class="col-xs-12 col-sm-4">
			<div class="img-thumbnail">
				<img src="${images }/${product.code}.jpg" style="width: 100%;" />
			</div>
		</div>

		<!-- display the product information -->
		<div class="col-xs-12 col-sm-6">
			<h3>${product.name }</h3>
			<hr>
			<span class="glyphicons glyphicons-tags"></span>

			<p>
				Time last: <span id="timer"></span>
			</p>
			<p>
				Current Price: <strong> &#36; ${product.currentPrice }</strong>
			</p>
			<p>
				Primary Price: <strong> &#36; ${product.initPrice }</strong>
			</p>
			<c:choose>
				<c:when test="${product.productCondition == '0'}">
					<p>Product Condition: Brand New</p>
				</c:when>
				<c:when test="${product.productCondition == '1'}">
					<p>Product Condition: Used</p>
				</c:when>
				<c:when test="${product.productCondition == '2'}">
					<p>Product Condition: Old</p>
				</c:when>
			</c:choose>
			<!-- <p>Product Condition: ${product.productCondition }</p> -->
			<p></p>


			<p>Start time: ${product.startDate }</p>


			<p>
				Deposit: <strong> &#36; ${product.initPrice*0.1 }</strong>
			</p>

			<c:if test="${not empty user }">
				<sf:form id="bidding-form"
					action="${contextRoot }/bid/${product.id }/user/${user.id}"
					method="POST" modelAttribute="biddingItem">
					<p>
						Bidding:
						<button type="button" id="minus_price"
							class="btn btn-warning btn-sm">-</button>
						<sf:input path="price" type="text" class="col-xs-2 col-sm-2"
							id="bidding_price" readonly="true" />
						<button type="button" id="add_price"
							class="btn btn-warning btn-sm">+</button>

					</p>
					<a href="${contextRoot }/buyitnow/${product.id }/user/${user.id}"
						class="btn btn-primary">Buy it now</a>
					<input type="submit" name="bidding-submit" id="bidding-submit"
						tabindex="4" class="btn btn-success" value="Bid and Pay Deposit">
					<p>
						You can add <strong> &#36; ${product.addAmount }</strong> to bid,
						or buy it now with <strong> &#36; ${product.fixedPrice }</strong>
					</p>
				</sf:form>
			</c:if>
		</div>
	</div>
	<script>
		window.startTime = '${product.startDate }';
		window.duration = '${product.duration }';
		window.currentPrice = '${currentPrice}';
		window.addAmount = '${product.addAmount}';
	</script>
	<div class="row">
		<div class="container col-xs-12 col-sm-10">

			<ul class="nav nav-tabs">
				<li class="nav-item"><a data-toggle="tab" href="#menu1"
					class="nav-link">Description</a></li>
				<li class="nav-item"><a data-toggle="tab" href="#menu2"
					class="nav-link">Bidding History</a></li>

			</ul>
			<div class="tab-content">
				<div id="menu1" class="tab-pane fade">
					<h3>Product Details</h3>
					<p>${product.description }</p>
				</div>
				<div id="menu2" class="tab-pane fade">
					<h3>Bidding History</h3>
					<table id="history" class="table table-striped table-bordered"
						cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Date</th>
								<th>Price</th>
								<th>Bidder</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Date</th>
								<th>Price</th>
								<th>Bidder</th>
							</tr>
						</tfoot>
						<tbody>
							<c:forEach items="${biddingItems }" var="biddingItem">
								<tr>
									<td>${biddingItem.formDate }</td>
									<td>${biddingItem.price }</td>
									<td>${biddingItem.userId }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</div>

			</div>
		</div>
	</div>
</div>