<%@include file="./flows/shared/login-navbar.jsp"%>

<div>
	<p></p>
	<h1 class="text-center">Welcome!</h1>
	<p></p>
</div>
<!-- content -->
<div class="container">
	<div class="row">
		<c:if test="${not empty message }">

			<div class="col-12 text-center">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&#xD7;</button>${message }</div>
			</div>
		</c:if>

		<div class="col-lg-6 mx-md-auto">
			<div class="card text-center">
				<div class="card-header">
					<ul class="nav nav-tabs card-header-tabs">
						<li class="nav-item col-12"><a class="nav-link active" href="#"
							id="register-form-link">Register</a></li>
					</ul>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-xl-12">
							<sf:form id="register-form" method="POST"
								action="${contextRoot }/register" modelAttribute="user">
								<div class="form-group">
									<sf:input type="text" path="username" id="username"
										tabindex="1" class="form-control" placeholder="Username"
										value="" />
									<sf:errors path="username" cssClass="help-block" element="em" />
								</div>
								<div class="form-group">
									<sf:input type="email" path="email" id="email" tabindex="1"
										class="form-control" placeholder="Email Address" value="" />
									<sf:errors path="email" cssClass="help-block" element="em" />
								</div>
								<div class="form-group">
									<sf:input type="password" path="password" id="password"
										tabindex="2" class="form-control" placeholder="Password" />
									<sf:errors path="password" cssClass="help-block" element="em" />
								</div>
								<div class="form-group">
									<sf:input type="password" path="confirmPassword"
										id="confirm-password" tabindex="2" class="form-control"
										placeholder="Confirm Password" />
									<sf:errors path="confirmPassword" cssClass="help-block"
										element="em" />
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6 mx-sm-auto">
											<input type="submit" name="register-submit"
												id="register-submit" tabindex="4"
												class="form-control btn btn-register" value="Register Now">

											<sf:hidden path="id" />
											<sf:hidden path="credit" />
											<sf:hidden path="avatar" />
										</div>
									</div>
								</div>
							</sf:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="./flows/shared/login-footer.jsp"%>