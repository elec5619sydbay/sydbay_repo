<div>
	<p></p>
	<h1 class="text-center">Login to Administrator System</h1>
	<p></p>
</div>
<!-- content -->
<div class="container">
	<div class="row">
		<c:if test="${not empty message }">

			<div class="col-12 text-center">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&#xD7;</button>${message }</div>
			</div>
		</c:if>

		<div class="col-lg-6 mx-md-auto">
			<div class="card text-center">
				<div class="card-body">
					<div class="row">
						<div class="col-xl-12">
							<sf:form id="login-form" action="${contextRoot }/adverify"
								method="POST" modelAttribute="admin" style="display: block;">
								<div class="form-group">
									<sf:input type="text" path="name" id="username"
										tabindex="1" class="form-control" placeholder="Username"
										value="" />
								</div>
								<div class="form-group">
									<sf:input type="password" path="password" id="password"
										tabindex="2" class="form-control" placeholder="Password" />
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6 mx-sm-auto">
											<input type="submit" name="login-submit" id="login-submit"
												tabindex="4" class="form-control btn btn-login"
												value="Log In">
											<sf:hidden path="id" />
										</div>
									</div>
								</div>
							</sf:form>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
