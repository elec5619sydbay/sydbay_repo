<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<div class="container">
	<c:if test="${not empty message }">
		<div class="col-12">
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				${message}
			</div>
		</div>
	</c:if>
	<div class="row">

		<div class="col-lg-12">

			<h1 class="my-4 font-italic">My Account</h1>
			<%-- 
			<a class="nav-link" href="${contextRoot}/json/data/user/${user.id}">My
				Profile</a>  --%>
			<div class="row">
				<div class="col-md-6">
					<a class="btn btn-outline-success" href="${contextRoot}/manage/products">Start
						a bidding</a> <a class="btn btn-outline-success"
						href="${contextRoot}/user/${user.id}/myorder">My order</a>
				</div>
				<div class="col-md-6">
					<div id="myAcc">
						<h5>Username:${user.username }</h5>
						<h5>Email:${user.email }</h5>
						<h5>Password:******</h5>
						<h5>credit:${user.credit }</h5>
					</div>

					<button type="button" class="btn btn-warning btn-xs"
						data-toggle="modal" data-target="#myAccountModal">Edit
						Profile</button>
				</div>
			</div>
		</div>

		<!-- /.col-lg-3 -->
	</div>
	<!-- /.row -->

	<!-- Add place the button -->
	<!-- Modal -->
	<div class="modal fade" id="myAccountModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
				</div>
				<div class="modal-body">

					<sf:form id="categoryForm" class="form-horizontal"
						modelAttribute="user" action="${contextRoot}/account/userdetail"
						method="POST">

						<div class="form-group">
							<label class="control-label col-md-4">Username:</label>
							<div class="col-md-8 validate">
								<sf:input type="text" path="username" class="form-control"
									placeholder="Username" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4">Email:</label>
							<div class="col-md-8 validate">
								<sf:input path="email" class="form-control" placeholder="Email" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Password:</label>
							<div class="col-md-8 validate">
								<sf:input path="password" class="form-control" type="password"
									placeholder="Password" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-8">Confirm Password:</label>
							<div class="col-md-8 validate">
								<sf:input path="confirmPassword" class="form-control" type="password"
									placeholder="Confirm Password" />
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-4">
								<input type="submit" name="submit" value="Update"
									class="btn btn-primary" />
							</div>
						</div>
					</sf:form>
				</div>
			</div>
		</div>
	</div>

</div>

</div>
<!-- /.container -->