<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="${contextRoot }/home">SYDBay</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<c:if test="${not empty adminacc }">
					<li class="nav-item" id="home"><a class="nav-link"
						href="${contextRoot }/adminhome">Home <span class="sr-only">(current)</span>
					</a></li>
				</c:if>
				<c:if test="${empty adminacc }">
					<li class="nav-item" id="home"><a class="nav-link"
						href="${contextRoot }/home">Home <span class="sr-only">(current)</span>
					</a></li>

					<li class="nav-item" id="listProducts"><a class="nav-link"
						href="${contextRoot}/show/all/products">View Products</a></li>

					<c:if test="${not empty user }">
						<li class="nav-item" id="myaccount"><a class="nav-link"
							href="${contextRoot}/myaccount">My Account</a></li>

						<li class="nav-item"><a class="nav-link"
							href="${contextRoot}/logout">Logout</a></li>
					</c:if>

					<c:if test="${empty user }">
						<li class="nav-item"><a class="nav-link"
							href="${contextRoot }/login">Login</a></li>
							<li class="nav-item"><a class="nav-link"
							href="${contextRoot }/register">Register</a></li>
					</c:if>
				</c:if>
			</ul>

		</div>
	</div>
</nav>
<c:if test="${empty adminacc }">
	<div class="row">
		<div class="col-10"></div>
		<!-- message button -->
		<a href="${contextRoot }/msg" class="btn btn-danger">Leave Message</a>
	</div>
</c:if>