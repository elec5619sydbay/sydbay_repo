
<div class="col-lg-2">

	<h1 class="my-4">SYDBay</h1>
	<div class="list-group">
		<c:forEach items="${categories }" var="category">
			<a href="${contextRoot}/category/${category.id}/products"
				class="list-group-item" id="c_${category.name }">${category.name }</a>
		</c:forEach>
	</div>

</div>