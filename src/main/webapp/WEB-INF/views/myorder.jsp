<div class="container">

	<div class="row">

		<div class="col-lg-12">

			<h1 class="my-4">My Order</h1>
											<div class="row">
							<div class="col-12">
								<table id="userOrderTable"
									class="table table-striped table-borderd">
									<thead>
										<tr>
											<th></th>
											<th>Name</th>
											<th>Duration</th>
											<th>Initial Price</th>
											<th>Add Amount</th>
											<th>Buy-it-now Price</th>
											<th>Current Price</th>
											<th>Purchase</th>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<th></th>
											<th>Name</th>
											<th>Duration</th>
											<th>Initial Price</th>
											<th>Add Amount</th>
											<th>Buy-it-now Price</th>
											<th>Current Price</th>
											<th>Purchase</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>

		</div>
		<!-- /.col-lg-3 -->
	</div>
	<!-- /.row -->

</div>
<!-- /.container -->
