<%@include file="../shared/login-navbar.jsp"%>
<div>
	<p></p>
	<h1 class="text-center">Welcome!</h1>
	<p></p>
</div>
<!-- content -->
<div class="container">
	<div class="row">
		<div class="col-lg-6 mx-md-auto">
			<div class="card text-center">
				<div class="card-header">
					<ul class="nav nav-tabs card-header-tabs">
						<li class="nav-item col-12"><a class="nav-link active"
							href="#" id="register-form-link">Register</a></li>
					</ul>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-xl-12">
							<sf:form id="register-form" method="POST" role="form"
								modelAttribute="user" style="display: block;">
								<div class="form-group">
									<sf:input type="text" path="username" name="username"
										id="username" tabindex="1" class="form-control"
										placeholder="Username" value="" />
									
								</div>
								<div class="form-group">
									<sf:input type="email" path="email" name="email" id="email"
										tabindex="1" class="form-control" placeholder="Email Address"
										value="" />
									
								</div>
								<div class="form-group">
									<sf:input type="password" path="password" name="password"
										id="password" tabindex="2" class="form-control"
										placeholder="Password" />
									
								</div>
								<div class="form-group">
									<sf:input type="password" path="confirmPassword"
										name="confirm-password" id="confirm-password" tabindex="2"
										class="form-control" placeholder="Confirm Password" />
									
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6 mx-sm-auto">
											<a href="${flowExecutionUrl}&_eventId=success"
												class="form-control btn btn-register">Register Now</a>
										</div>
									</div>
								</div>
							</sf:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="../shared/login-footer.jsp"%>