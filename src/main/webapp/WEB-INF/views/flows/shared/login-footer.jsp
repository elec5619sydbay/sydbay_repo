
	<!-- footer -->
	<footer class="py-5 bg-dark footer">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; Sydbay 2017</p>
		</div>
		<!-- /.container -->
	</footer>

	<!-- Bootstrap core JavaScript -->
	<script src="${js }/jquery.min.js"></script>
	<script src="${js }/jquery.validate.js"></script>
	<script src="${js }/bootstrap.min.js"></script>
	<script src="${js }/bootstrap.js"></script>
	<script src="${js }/myapp.js"></script>
	<script>
		$('#login-form-link').click(function(e) {
			$("#login-form").delay(100).fadeIn(100);
			$("#register-form").fadeOut(100);
			$('#register-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		$('#register-form-link').click(function(e) {
			$("#register-form").delay(100).fadeIn(100);
			$("#login-form").fadeOut(100);
			$('#login-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		
		var $alert = $('.alert');
		if($alert.length){
			setTimeout(function(){
				$alert.fadeOut('slow');
			},3000)
		}
	</script>
</body>
</html>