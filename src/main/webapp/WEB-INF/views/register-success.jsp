<%@include file="./flows/shared/login-navbar.jsp"%>
<div class="container">
	<div class="row">
		<div class="mx-sm-auto col-md-4">
			<div class="text-center">
				<h1>Welcome!</h1>
				<h3>SYDBay for students in the University of Sydney</h3>
				<h6>You can use your username to login!</h6>
				<div>
					<a href="${contextRoot}/login" class="btn btn-lg btn-success">Login
						Here</a>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="./flows/shared/login-footer.jsp"%>