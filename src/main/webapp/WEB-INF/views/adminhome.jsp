<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="sydbay/rescources/myapp.js"></script>

<div class="container">

	<div class="row">
		<c:if test="${not empty message}">
			<div class="col-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					${message}
				</div>
			</div>
		</c:if>
		
	</div>
	
<!-- Product table for Admin -->
	<div class="row">
		<div class="col-12">
			<h3>Available Products</h3>
			<hr />
		</div>
		<div class="col-12">
			<table id="adminProductsList" class="table table-sm table-bordered">
				<thead>
					<tr>
						<th>Id</th>
						<th>Image</th>
						<th>Name</th>
						<th>duration</th>
						<th>Initial Price</th>
						<th>Add Amount</th>
						<th>Fixed Price</th>
						<th>Current Price</th>
						<th>Active</th>
						<th>Edit</th>
					</tr>
				</thead>


				<tfoot>
					<tr>
						<th>Id</th>
						<th>Image</th>
						<th>Name</th>
						<th>duration</th>
						<th>Initial Price</th>
						<th>Add Amount</th>
						<th>Fixed Price</th>
						<th>Current Price</th>
						<th>Active</th>
						<th>Edit</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>