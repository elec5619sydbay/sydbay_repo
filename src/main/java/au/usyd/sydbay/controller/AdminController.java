package au.usyd.sydbay.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.sydbay.domain.AdminAccount;
import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.service.AdminServices;

@Controller
public class AdminController {

	@Autowired
	private HttpSession session;

	@Autowired
	AdminServices adserver;

	@RequestMapping(value = "/adminlogin", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "adverify", required = false) String adverify) {
		ModelAndView a = new ModelAndView("adminpage");

		AdminAccount ad = new AdminAccount();

		a.addObject("admin", ad);
		a.addObject("title", "Admin Login");
		a.addObject("userClickAdminLogin", true);
		if (adverify != null) {
			if (adverify.equals("Username already exist!")) {
				a.addObject("message", "Username already exist! Please login or use another username to register");
			}
			if (adverify.equals("User successfully added")) {
				a = new ModelAndView("register-success");
			}
			if (adverify.equals("Invalid username or password")) {
				a.addObject("message", "Invalid username! Please Sign up");

			}
			if (adverify.equals("wrongpassword")) {
				a.addObject("message", "The password is incorrect");

			}
		}

		return a;
	}

	@RequestMapping(value = "/adverify", method = RequestMethod.POST)
	public String signin(@ModelAttribute("adminacc") AdminAccount ad) {

		AdminAccount compare = adserver.getAdById(ad.getName());
		if (compare == null) {
			return "redirect:/adminlogin?adverify=" + "Invalid name or password";
		}
		if (!compare.getPassword().equals(ad.getPassword())) {
			return "redirect:/adminlogin?adverify=" + "wrongpassword";
		}
		session.setAttribute("adminacc", compare);
		System.out.println("admin: "+session.getAttribute("adminacc"));
		return "redirect:/adminhome";

	}

	//test the userservice
	/*	@RequestMapping(value = { "/adminlogin"})
	public ModelAndView adminlogin() {
		ModelAndView mv1 = new ModelAndView("adminlogin");
		User user = new User();
		user.setUsername("zara");
		user.setPassword("123");
		user.setEmail("123");
		user.setAvatar(1);
		user.setCredit(1);
		int test = 2;
		userService.DeleteUser(test);	
		return mv1;
	}*/

	@RequestMapping(value = "/adminhome", method = RequestMethod.GET)
	public ModelAndView showManageProducts(@RequestParam(value = "operation", required = false) String operation) {
		ModelAndView mv = new ModelAndView("adminpage");
		mv.addObject("userClickAdminHome", true);
		mv.addObject("title", "Manage Products");

		Product nProduct = new Product();
		nProduct.setSupplierId(1);
		nProduct.setStatus(0);
		nProduct.setActive(true);
		mv.addObject("product", nProduct);

		if (operation != null) {
			if (operation.equals("product")) {
				mv.addObject("message", "Product Submitted Successfully!");
			}
		}
		mv.addObject("title", "Admin Home");
		mv.addObject("adminacc", session.getAttribute("adminacc"));
		return mv;
	}
	
	@RequestMapping(value="/adminlogout")
	public String logout(){
		
		session.removeAttribute("adminacc");
		return "redirect:/adminlogin?operation="+"logout";
	}
}
