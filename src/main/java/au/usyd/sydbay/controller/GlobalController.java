package au.usyd.sydbay.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import au.usyd.sydbay.domain.User;
import au.usyd.sydbay.model.UserModel;
import au.usyd.sydbay.service.UserService;

@ControllerAdvice
public class GlobalController {

	@Autowired
	private HttpSession session;
	
	@Autowired
	private UserService userService;
	
	private UserModel userModel = null;
	
	@ModelAttribute("userModel")
	public UserModel getUserModel(){
		if(session.getAttribute("userModel") == null){
			//add the user model
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			User user = userService.findByUsername(authentication.getName());
			
			if(user!=null){
				//create a new user model to pass the user details
				userModel = new UserModel();
				
				userModel.setId(user.getId());
				userModel.setEmail(user.getEmail());
				userModel.setUsername(user.getUsername());
				
				session.setAttribute("userModel", userModel);
				
				return userModel;
			}
		}
		return (UserModel) session.getAttribute("userModel");
	}
}
