package au.usyd.sydbay.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.sydbay.domain.BiddingItem;
import au.usyd.sydbay.domain.OrderItem;
import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.service.BiddingService;
import au.usyd.sydbay.service.OrderService;
import au.usyd.sydbay.service.ProductService;
import au.usyd.sydbay.service.UserService;

@Controller
public class BiddingOrderController {

	@Autowired
	OrderService orderService;
	
	@Autowired
	BiddingService biddingService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	UserService userService;
	@Autowired
	private HttpSession session;

	/*
	 * buy it now
	 * simply tell user, they successfully add an order
	 * */
		@RequestMapping(value = "/buyitnow/{productid}/user/{userid}")
		public String buyItNow(@PathVariable("productid") int productId, @PathVariable("userid") int userId) {
			
			OrderItem orderItem = new OrderItem();
			orderItem.setFormDate(new Date());
			orderItem.setOrderStatus(0);
			orderItem.setProductId(productId);
			orderItem.setUserId(userId);
			orderService.addOrderItem(orderItem);
			
			return "redirect:/show/"+productId+"/product?operation=buysuccess";
		}
		
		
		@RequestMapping(value = "/bid/{productid}/user/{userid}")
		public String Bid(@PathVariable("productid") int productId, @PathVariable("userid") int userId, @ModelAttribute("biddingItem") BiddingItem biddingitem) {
			
			
			biddingitem.setFormDate(new Date());
			biddingitem.setProductId(productId);
			biddingitem.setUserId(userId);
			
			if(productService.getById(productId).getInitPrice()<biddingitem.getPrice()){
				biddingService.addBiddingItem(biddingitem);
				Product product=productService.getById(productId);
				product.setCurrentPrice(biddingitem.getPrice());
				productService.updateProduct(product);
				return "redirect:/show/"+productId+"/product?operation=bidsuccess";
				
			}
			
			
			
			return "redirect:/show/"+productId+"/product?operation=priceerror";
			
		}
		

		// my order page
		@RequestMapping(value = { "/user/{id}/myorder" })
		public ModelAndView myOrder(@PathVariable("id") int id) {
			ModelAndView mv = new ModelAndView("page");
			mv.addObject("title", "My Order");
			mv.addObject("userClickOrder", true);

			mv.addObject("orders", orderService.findOrderByUserId(id));

			mv.addObject("user", session.getAttribute("user"));
			return mv;
		}
}
