package au.usyd.sydbay.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.sydbay.domain.User;
import au.usyd.sydbay.service.UserService;


@Controller
public class UserLoginController {

	@Autowired
	UserService userService;
	@Autowired
	private HttpSession session;
	
	private static final Logger logger= LoggerFactory.getLogger(ManagementController.class);
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public ModelAndView login(@RequestParam(value="operation",required=false) String operation){
		ModelAndView mv = new ModelAndView("login");
		
		User user =new User();
		//initialize user some information
		user.setAvatar(0);
		user.setCredit(0);
		user.setEmail("a@c");
		user.setConfirmPassword("1");
		
		mv.addObject("user", user);
		
		if(operation!=null){
			if(operation.equals("Username already exist!")){
				mv.addObject("message", "Username already exist!");
			}
			if(operation.equals("User successfully added")){
				mv = new ModelAndView("register-success");
			}
			if(operation.equals("invalid")){
				mv.addObject("message", "Invalid username! Please Sign up");

			}
			if(operation.equals("wrongpassword")){
				mv.addObject("message", "The password is incorrect");

			}
			if(operation.equals("logout")){
				mv.addObject("message", "You have successfully logged out.");

			}
		}
		
		mv.addObject("title", "Login");
		
		return mv;
	}
	
	@RequestMapping(value="/register",method=RequestMethod.GET)
	public ModelAndView register(@RequestParam(value="operation",required=false) String operation){
		
		ModelAndView mv = new ModelAndView("register");
		
		User user =new User();
		//initialize user some information
		user.setAvatar(0);
		user.setCredit(0);
		
		mv.addObject("user", user);
		
		if(operation!=null){
			if(operation.equals("Username already exist!")){
				mv.addObject("message", "Username already exist! Please login or use another username to register");
			}
			if(operation.equals("User successfully added")){
				mv = new ModelAndView("register-success");
			}
			if(operation.equals("invalid")){
				mv.addObject("message", "Invalid username! Please Sign up");

			}
			if(operation.equals("wrongpassword")){
				mv.addObject("message", "The password is incorrect");

			}
			
		}
		
		mv.addObject("title", "Register");
		
		return mv;
	}
	
	@RequestMapping(value="/signin", method=RequestMethod.POST)
	public String signin(@Valid @ModelAttribute("user") User nUser, BindingResult results, Model model){
		System.out.println("login function");
		if(results.hasErrors()){
			model.addAttribute("title", "Login");
			return "login";
		}
		
		User user = userService.findByUsername(nUser.getUsername());
		if(user == null){
			return "redirect:/login?operation="+"invalid";
		}
		if(!user.getPassword().equals(nUser.getPassword())){
			return "redirect:/login?operation="+"wrongpassword";
		}
		session.setAttribute("user", user);
		System.out.println(session.getAttribute("user"));
		
		return "redirect:/home";
		
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String register(@Valid @ModelAttribute("user") User nUser, BindingResult result, Model model){
		System.out.println("register function");
		if(result.hasErrors()){
			
			model.addAttribute("title","Register");
			return "register";
		}
		
		//logger.info(nUser.toString());
	
		return "redirect:/register?operation="+userService.addUser(nUser);
	}
	
	@RequestMapping(value="/logout")
	public String logout(){
		
		session.removeAttribute("user");
		return "redirect:/login?operation="+"logout";
	}
}
