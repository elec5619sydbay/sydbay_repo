package au.usyd.sydbay.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.domain.User;
import au.usyd.sydbay.service.UserService;
//import au.usyd.sydbay.utils.*;

@Controller
@RequestMapping(value = "/account")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private HttpSession session;



//	@RequestMapping(value = "/home", method = RequestMethod.GET)
//	public String viewAccount() {
//		return "account_home";
//	}
//
//	@RequestMapping(value = "/logout", method = RequestMethod.GET)
//	public String logOut(HttpServletRequest request) {
//		request.getSession().removeAttribute("user");
//		return "home";
//	}
	
	
	
	
	
//The method is for update an existing user
	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String updateUser(HttpServletRequest request, ModelMap model){
		String username = request.getParameter("username");
		User user = userService.getUser(username);
		if(username != null){
			user = userService.getUser(username);
		}
		if(user != null) {
			return "account_set";
		}
		return null;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String processUpdate(@RequestParam("password") String password, @RequestParam("newPassword1") String newPassword1,
			@RequestParam("newPassword2") String newPassword2,@RequestParam("email") String email, @RequestParam("avatar") int avatar, @RequestParam
			HttpServletRequest request) {
		Map<String, String> userInfo = new HashMap<String, String>();
		userInfo.put("password", password);
		userInfo.put("newPassword1", newPassword1);
		userInfo.put("newPassword2", newPassword2);
		userInfo.put("email", email);
		
		String username = request.getParameter("username");
		User user = null;
		if(username != null){
			user = userService.getUser(username);
		}
		if(user != null) {
			if(userService.updateUser(user, userInfo)){
				request.getSession().setAttribute("user", user);
				return "account_home";
			}
			return "account_set_failure";
		}
		return null;
	}
	
	//list user account information
	 @RequestMapping(value="/account_list",method = RequestMethod.GET)
	    public String listUsers(ModelMap model, HttpServletRequest request) {
		 	List<User> users = userService.findAllUser();
	        model.addAttribute("users", users);
	        return "account_list";
	    }
	 
	//delete user account
	@RequestMapping(value = "/account_delete", method = RequestMethod.GET)
	public String deleteAccount(HttpServletRequest request){
		String username = request.getParameter("username");
		userService.deleteUser(username);
		//need to modify
	    return "redirect:./account_list";
	}
	
	@RequestMapping(value="/json/data/user/{id}",produces = "application/json", method=RequestMethod.GET)
	@ResponseBody
	public User getUserById(@PathVariable int id) {
		return userService.findById(id);
	}
	
	@RequestMapping(value="/userdetail", method=RequestMethod.POST)
	public String updateUser(@ModelAttribute("user") User mUser, BindingResult result,
			Model model, HttpServletRequest request) {
		
		//logger.info(mProduct.toString());
		
		//mUser.setId(userService.findByUsername(mUser.getUsername()).getId());
		//System.out.println(mUser);
		// create a new product record
		//ProductDAO.add(mProduct);
		User user = (User)session.getAttribute("user");
		user = userService.findById(user.getId());
		user.setUsername(mUser.getUsername());
		user.setEmail(mUser.getEmail());
		user.setPassword(mUser.getPassword());
		user.setConfirmPassword("1");
		System.out.println("update user:"+user);
		
		userService.update(user);
		
		return "redirect:/myaccount?operation=success";
	}
//	//handle the submission event
//	@RequestMapping(value = "/category", method=RequestMethod.POST)
//	public String managePostCategory(@ModelAttribute("category") Category mCategory, HttpServletRequest request) {					
//		categoryDAO.add(mCategory);		
//		return "redirect:" + request.getHeader("Referer") + "?success=category";
//	}
//	
//	
//	
//	@ModelAttribute("categories") 
//	public List<User> modelCategories() {
//		return UserDAO.list();
//	}
//	
//	@ModelAttribute("category")
//	public Category modelCategory() {
//		return new Category();
//	}
//	
	

}
