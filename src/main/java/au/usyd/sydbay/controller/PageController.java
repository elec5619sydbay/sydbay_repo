package au.usyd.sydbay.controller;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.webflow.engine.model.Model;

import au.usyd.sydbay.dao.MessageReplyService;
import au.usyd.sydbay.dao.MessageService;
import au.usyd.sydbay.domain.BiddingItem;
import au.usyd.sydbay.domain.Category;
import au.usyd.sydbay.domain.Message;
import au.usyd.sydbay.domain.MessageReply;
import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.domain.User;
import au.usyd.sydbay.service.AdminServices;
import au.usyd.sydbay.service.BiddingService;
import au.usyd.sydbay.service.CategoryService;
import au.usyd.sydbay.service.OrderService;
import au.usyd.sydbay.service.ProductService;
import au.usyd.sydbay.service.UserService;
import au.usyd.sydbay.vo.ListVo;

/**
 * Handles requests for the application home page.
 */
@Controller
public class PageController {
	@Autowired
	CategoryService categoryService;

	@Autowired
	ProductService productService;

	@Autowired
	AdminServices adserver;

	@Autowired
	BiddingService biddingService;

	@Autowired
	UserService userService;


	@Autowired
	OrderService orderService;
	
	@Autowired
	MessageReplyService messageReplyService;

	@Autowired
	private HttpSession session;

	@RequestMapping(value = { "/", "/home", "index" })
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "Home");
		mv.addObject("userClickHome", true);

		// pass all products
		mv.addObject("products", productService.listAllActiveProducts());
		// pass category list
		mv.addObject("categories", categoryService.getAllCategories());

		mv.addObject("user", session.getAttribute("user"));
		return mv;
	}

	// my account page
	@RequestMapping(value = "/myaccount", method = RequestMethod.GET)
	public ModelAndView myAccount(@RequestParam(value="operation", required=false) String operation) {
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "My account");
		mv.addObject("userClickAcc", true);
		User user = (User) session.getAttribute("user");
		mv.addObject("user", userService.findById(user.getId()));
		
		if(operation!=null){
			mv.addObject("message", "Update personal detail successfully!");
		}
		return mv;
	}


	// view products page
	@RequestMapping(value = { "/show/all/products" })
	public ModelAndView showAllProducts() {
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "All Products");
		mv.addObject("userClickAllProducts", true);

		//pass all products
		mv.addObject("products", productService.listAllActiveProducts());
		// pass category list
		mv.addObject("categories", categoryService.getAllCategories());

		mv.addObject("user", session.getAttribute("user"));
		return mv;
	}
	
	// load all products base on category
	@RequestMapping(value = { "/category/{id}/products" })
	public ModelAndView showCategoryProducts(@PathVariable("id") int id) {

		// fetch a single category
		Category category = categoryService.getCategoryById(id);

		// get products belongs to that category
		List<Product> products = productService.getProductsByCategory(id);

		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", category.getName());
		mv.addObject("userClickCategory", true);

		// pass category list
		mv.addObject("categories", categoryService.getAllCategories());
		mv.addObject("category", category);

		// pass product list
		mv.addObject("products", products);

		mv.addObject("user", session.getAttribute("user"));
		return mv;
	}

	// viewing a single product
	@RequestMapping(value = "/show/{id}/product")
	public ModelAndView showSingleProduct(@PathVariable int id,
			@RequestParam(value = "operation", required = false) String operation) {
		ModelAndView mv = new ModelAndView("page");
		Product product = productService.getById(id);

		mv.addObject("title", product.getName());
		mv.addObject("product", product);
		mv.addObject("userClickShowProduct", true);
		mv.addObject("biddingItems", biddingService.getAllBiddingItemByProduct(id));
//		System.out.println(biddingService.getAllBiddingItemByProduct(id).size()+"-"+biddingService.getAllBiddingItemByProduct(id).toString());
//
//		float currentPrice = calculateCurrentPrice(biddingService.getAllBiddingItemByProduct(id), product.getInitPrice());
//		mv.addObject("currentPrice", currentPrice);
//		
		mv.addObject("user", session.getAttribute("user"));
		

		if (operation != null) {
			if (operation.equals("buysuccess")) {
				mv.addObject("message", "Your order submitted successfully, Please go to My order to checkout.");
			}
			if (operation.equals("bidsuccess")) {
				mv.addObject("message", "Congratulation! Bidding Application submitted successfully, Please go to My Bidding Item to checkout.");
			}if (operation.equals("priceerror")) {
				mv.addObject("message", "The price you bid must more than its current price.");
			}
		}

		BiddingItem biddingItem = new BiddingItem();
		biddingItem.setPrice(product.getCurrentPrice());
		mv.addObject("biddingItem", biddingItem);
		return mv;
	}

	//calculate current price by comparing biddingItem price and initial price
	private float calculateCurrentPrice(List<BiddingItem> items, float initPrice){
		
		if(items.size()>0){
			float tPrice = initPrice;
			for(BiddingItem item: items){
				if(tPrice<item.getPrice()){
					tPrice = item.getPrice();
				}
			}
			return tPrice;
		}
		
		return initPrice;
	}
	
	
	
	
	//message调用
	
	private static final Logger logger = LoggerFactory.getLogger(PageController.class);
		
		/**
		 * Simply selects the home view to render by returning its name.
		 */
		@RequestMapping(value = "/msg", method = RequestMethod.GET)
		public ModelAndView home() {
			ApplicationContext context = new ClassPathXmlApplicationContext("/au/usyd/sydbay/config/applicationContext.xml");
			MessageService messageService = (MessageService) context.getBean("messageService");
			
			List<ListVo> list = messageService.getMessage(1);
			//System.out.println(list.get(0).getMessage().getUsername()+"-===-=========");
			
			ModelAndView mv = new ModelAndView("index");
//			mv.addObject("userClickMsg", true);
			mv.addObject("list", list);
		
			
			return mv;
		}
		
		@RequestMapping(value = "/message",method = RequestMethod.GET)
		public ModelAndView Message(@ModelAttribute("message")Message message,BindingResult result,RedirectAttributes redirectAttributes){
			ApplicationContext context = new ClassPathXmlApplicationContext("/au/usyd/sydbay/config/applicationContext.xml");
			MessageService messageService = (MessageService) context.getBean("messageService");
			
			Date date = new Date();
			message.setTime(date);
			messageService.saveMessage(message);
			return new ModelAndView("redirect:/msg");
		}
		
		
		@RequestMapping(value = "/reply",method = RequestMethod.GET)
		public ModelAndView reply(@ModelAttribute("messageReply")MessageReply messageReply,BindingResult result,RedirectAttributes redirectAttributes,HttpServletRequest request){
			ApplicationContext context = new ClassPathXmlApplicationContext("/au/usyd/sydbay/config/applicationContext.xml");
			MessageService messageService = (MessageService) context.getBean("messageService");
			MessageReplyService replyService = (MessageReplyService) context.getBean("messageReplyService");
			
			Message m = messageService.searchMessage(messageReply.getId());
			
			MessageReply mr = new MessageReply();
			Date date = new Date();
			mr.setUsername(messageReply.getUsername());
			mr.setContent(messageReply.getContent());
			mr.setTime(date);
			mr.setMessage(m);
			
			replyService.saveMessageReply(mr);
			
			return new ModelAndView("redirect:/msg");
		}
		
		@RequestMapping(value = "/rdelete")                                  //删除回复
		public ModelAndView rDelete(HttpServletRequest request){
			System.out.println("-----------------------"+request.getParameter("id"));
			long id = Long.parseLong(request.getParameter("id"));
			long mid = Long.parseLong(request.getParameter("mid"));
			
			ApplicationContext context = new ClassPathXmlApplicationContext("/au/usyd/sydbay/config/applicationContext.xml");
			MessageReplyService messageReplyService = (MessageReplyService) context.getBean("messageReplyService");
			messageReplyService.delReply(id);
			messageReplyService.deleteReply(id);
			
			return new ModelAndView("redirect:/msg");
		}
		
		
		@RequestMapping(value = "/mdelete")                                 //删除留言
		public ModelAndView mdelete(HttpServletRequest request){
			System.out.println("-----------------------"+request.getParameter("id"));
			long id = Long.parseLong(request.getParameter("id"));
			
			
			ApplicationContext context = new ClassPathXmlApplicationContext("/au/usyd/sydbay/config/applicationContext.xml");
			MessageService messageService = (MessageService) context.getBean("messageService");
			
			messageService.delMessage(id);
			return new ModelAndView("redirect:/msg");
		}
		
		
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test(Locale locale, Model model) {

		User user = new User();
		user.setUsername("zara");
		user.setPassword("123");
		user.setEmail("123");
		user.setAvatar(1);
		
		userService.addUser(user);
		
		return "page";
	}
	

}
