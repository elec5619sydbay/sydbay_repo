package au.usyd.sydbay.controller;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.sydbay.dao.CategoryDao;
import au.usyd.sydbay.dao.ProductDao;
import au.usyd.sydbay.domain.Category;
import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.util.FileUploadUtility;
import au.usyd.sydbay.validator.ProductValidator;

@Controller
@RequestMapping("/manage")
public class ManagementController {

	@Autowired
	private CategoryDao categoryDAO;

	@Autowired
	private ProductDao ProductDAO;
	
	@Autowired
	private HttpSession session;


	private static final Logger logger = LoggerFactory.getLogger(ManagementController.class);

	// show products management
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ModelAndView showManageProducts(@RequestParam(value = "operation", required = false) String operation) {
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("userClickManageProducts", true);
		mv.addObject("title", "Manage Products");

		Product nProduct = new Product();
		nProduct.setSupplierId(1);
		nProduct.setStatus(0);
		nProduct.setActive(true);
		mv.addObject("product", nProduct);

		if (operation != null) {
			if (operation.equals("product")) {
				mv.addObject("message", "Product Submitted Successfully!");
			} else if (operation.equals("category")) {
				mv.addObject("message", "Category Submitted Successfully!");
			}
		}
		mv.addObject("user", session.getAttribute("user"));
		mv.addObject("adminacc", session.getAttribute("adminacc"));

		return mv;
	}

	// product submission
	@RequestMapping(value = "/products", method = RequestMethod.POST)
	public String handleProductSubmission(@Valid @ModelAttribute("product") Product mProduct, BindingResult result,
			Model model, HttpServletRequest request) {

		model.addAttribute("adminacc", session.getAttribute("adminacc"));
		
		// Spring Validator
		if (mProduct.getId() == 0) {
			new ProductValidator().validate(mProduct, result);
		} else {
			if (!mProduct.getFile().getOriginalFilename().equals("")) {
				new ProductValidator().validate(mProduct, result);
				mProduct.setCode("PRD" + UUID.randomUUID().toString().substring(26).toUpperCase());
			} else {

			}
		}

		// check if there are the errors
		if (result.hasErrors()) {
			model.addAttribute("userClickManageProducts", true);
			model.addAttribute("title", "Manage Products");
			model.addAttribute("message", "Validation failed for product submission!");
			
			model.addAttribute("user", session.getAttribute("user"));
			model.addAttribute("adminacc", session.getAttribute("adminacc"));

			
			return "page";
		}

		logger.info(mProduct.toString());

		mProduct.setCurrentPrice(mProduct.getInitPrice());
		
		// Edit or create
		if (mProduct.getId() == 0) {
			// create a new product record
			ProductDAO.add(mProduct);
		} else {
			ProductDAO.update(mProduct);
		}

		if (!mProduct.getFile().getOriginalFilename().equals("")) {
			FileUploadUtility.uploadFile(request, mProduct.getFile(), mProduct.getCode());
		}

		return "redirect:/manage/products?operation=product";
	}

	@RequestMapping(value = "/{id}/product", method = RequestMethod.GET)
	public ModelAndView showEditProduct(@PathVariable int id) {

		ModelAndView mv = new ModelAndView("page");
		mv.addObject("userClickManageProducts", true);
		mv.addObject("title", "Manage Products");

		Product nProduct = ProductDAO.get(id);

		mv.addObject("product", nProduct);
		mv.addObject("adminacc", session.getAttribute("adminacc"));
		return mv;

	}

	@RequestMapping(value = "/product/{id}/activation", method = RequestMethod.POST)
	@ResponseBody
	public String managePostProductActivation(@PathVariable int id) {
		
		Product product = ProductDAO.get(id);
		boolean isActive = product.isActive();
		//System.out.println(isActive);
		product.setActive(!isActive);
		ProductDAO.update(product);
		return (isActive) ? "Product Dectivated Successfully! id = " + product.getId()
				: "Product Activated Successfully! id = " + product.getId();
	}

	// category submission
	@RequestMapping(value = "/category", method = RequestMethod.POST)
	public String managePostCategory(@ModelAttribute Category mCategory) {
		categoryDAO.add(mCategory);
		return "redirect:/manage/products?operation=category";
	}

	// return categories
	@ModelAttribute("categories")
	public List<Category> getCategories() {
		return categoryDAO.listAllCategory();
	}

	// set categories
	@ModelAttribute("category")
	public Category modelCategory() {
		return new Category();
	}

}