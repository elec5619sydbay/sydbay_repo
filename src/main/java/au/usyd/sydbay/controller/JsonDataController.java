package au.usyd.sydbay.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.usyd.sydbay.domain.OrderItem;
import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.domain.User;
import au.usyd.sydbay.service.OrderService;
import au.usyd.sydbay.service.ProductService;
import au.usyd.sydbay.service.UserService;

@Controller
@RequestMapping("/json/data")
public class JsonDataController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value="/all/products", produces = "application/json", method=RequestMethod.GET)
	@ResponseBody
	public List<Product> getAllProducts() {
		return productService.listAllActiveProducts();
	}
	
	@RequestMapping(value="/category/{id}/products", produces = "application/json", method=RequestMethod.GET)
	@ResponseBody
	public List<Product> getProductsByCategory(@PathVariable int id) {
		return productService.getActiveProductsByCategory(id);
	}
	
	@RequestMapping("/admin/all/products")
	@ResponseBody
	public List<Product> getAllProductsForAdmin() {		
		return productService.getAllProducts();
	}
	
	@RequestMapping(value="/test", method=RequestMethod.GET)
	@ResponseBody
	public String getTest() {
		return "test";
	}
	
	@RequestMapping("/admin/user")
	@ResponseBody
	public List<User> getAllUsersForAdmin() {		
		return userService.getAllUsers();
	}
	
	@RequestMapping(value="/user/{id}/orders", produces = "application/json", method=RequestMethod.GET)
	@ResponseBody
	public List<Product> findAllOrderItemsByUserId(@PathVariable int id) {
		return orderService.findBiddingProducts(id);
	}
}
