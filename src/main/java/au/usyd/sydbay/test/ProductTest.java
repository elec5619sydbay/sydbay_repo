package au.usyd.sydbay.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import au.usyd.sydbay.dao.ProductDao;
import au.usyd.sydbay.dao.ProductDaoImpl;
import au.usyd.sydbay.domain.Product;
import junit.framework.TestCase;

public class ProductTest extends TestCase {

	private Product product;
	
	private ProductDao productDao;

	protected void setUp() throws Exception {
		
		product = new Product();
		productDao = new ProductDaoImpl();
		
	}
	
	
	@Test
	public void testCUSDProduct() {
		
		product.setName("test");
		product.setActive(true);
		product.setAddAmount(5);
		product.setCategoryId(1);
		product.setDuration(2);
		product.setProductCondition(0);
		product.setFixedPrice(100);
		product.setInitPrice(50);
		product.setStatus(1);
		product.setSupplierId(0);
		//add product
		productDao.add(product);
		//update
		product.setActive(false);
		productDao.update(product);
		//select
		assertEquals(2, productDao.listActiveProducts().size());
		//delete
		productDao.delete(product);
		
	}
}
