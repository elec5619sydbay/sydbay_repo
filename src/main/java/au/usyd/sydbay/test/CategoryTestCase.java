package au.usyd.sydbay.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import au.usyd.sydbay.domain.Category;
import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.service.CategoryService;
import au.usyd.sydbay.service.ProductService;
import junit.framework.TestCase;

public class CategoryTestCase extends TestCase {

	private Product product;
	private Category category;
	private CategoryService cService;
	private ProductService pService;

	protected void setUp() throws Exception {
		cService = new CategoryService();
		category = new Category();
		product = new Product();
	}

	/*
	 * @Test public void testAddCategory() { category.setName("TestCtg");
	 * assertEquals(true, cService.addCategory(category)); }
	 */

	@Test
	public void testSetProductName() {
		product.setCategoryId(0);
		assertEquals(0, product.getCategoryId());
	}
	
	/*	@Test
	public void testCRUDProduct() {
		
		// create operation
		product = new Product();
				
		product.setName("Oppo Selfie S53");
		product.setBrand("Oppo");
		product.setDescription("This is some description for oppo mobile phones!");
		product.setUnitPrice(25000);
		product.setActive(true);
		product.setCategoryId(3);
		product.setSupplierId(3);
		
		assertEquals("Something went wrong while inserting a new product!",
				true,productDAO.add(product));		
		
		
		// reading and updating the category
		product = productDAO.get(2);
		product.setName("Samsung Galaxy S7");
		assertEquals("Something went wrong while updating the existing record!",
				true,productDAO.update(product));		
				
		assertEquals("Something went wrong while deleting the existing record!",
				true,productDAO.delete(product));		
		
		// list
		assertEquals("Something went wrong while fetching the list of products!",
				6,productDAO.list().size());		
				
	}
		*/	
	
/*	@Test
	public void testListActiveProducts() {
		assertEquals("Something went wrong while fetching the list of products!",
				5,pService.listAllActiveProducts().size());				
	} 
*/	
	
/*	@Test
	public void testListActiveProductsByCategory() {
		assertEquals("Something went wrong while fetching the list of products!",
				3,pService.getProductsByCategory(3).size());
	}
*/ 
	
/*	@Test
	public void testCRUDCategory() {
		
		// add operation
		category = new Category();
		
		category.setName("Laptop");
		
		assertEquals("Successfully added a category inside the table!",true,cService.addCategory(category));
		
		
		category = new Category();
		
		category.setName("Television");

		
		assertEquals("Successfully added a category inside the table!",true,cService.addCategory(category));

		
		// fetching and updating the category
		category = cService.getCategoryById(2);
		
		category.setName("TV");
		
		assertEquals("Successfully updated a single category in the table!",true,cService.updateCategory(category));
		
	}
*/	
}