package au.usyd.sydbay.dao;

import java.util.List;

import au.usyd.sydbay.domain.OrderItem;
import au.usyd.sydbay.domain.Product;

public interface OrderDao {

	List<OrderItem> findAllOrderItemsByUserId(int id);
	
	void addOrderItem(OrderItem orderItem);
	
	List<Product> findBiddingProducts(int id);
}
