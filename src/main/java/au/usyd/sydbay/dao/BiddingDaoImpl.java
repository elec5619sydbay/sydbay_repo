package au.usyd.sydbay.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.sydbay.domain.BiddingItem;

@Repository("biddingDao")
@Transactional
public class BiddingDaoImpl implements BiddingDao {

	@Resource
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<BiddingItem> findAllBiddingItemByProductId(int id) {
		Query q = sessionFactory.getCurrentSession().createQuery("from BiddingItem where productId = :id");
		q.setInteger("id", id);
		return (List<BiddingItem>) q.list();
	}

	@Override
	public void add(BiddingItem biddingItem) {
		sessionFactory.getCurrentSession().persist(biddingItem);
	}

}
