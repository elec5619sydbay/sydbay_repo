package au.usyd.sydbay.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.sydbay.domain.Product;

@Repository("ProductDao")
@Transactional
public class ProductDaoImpl implements ProductDao {

	@Resource
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Product> listAllProduct() {
		List<Product> products;
		Query query = sessionFactory.getCurrentSession().createQuery("from Product");

		products = (List<Product>) query.list();
		
		return products;
	}

	@Override
	public List<Product> listProductByCategory(int id) {
		Query q = sessionFactory.getCurrentSession().createQuery("from Product where categoryId = :id");
		q.setInteger("id", id);
		return (List<Product>) q.list();
	}
	
	@Override
	public List<Product> listActiveProductByCategory(int id) {
		Query q = sessionFactory.getCurrentSession().createQuery("from Product where categoryId = :id and active = :active");
		q.setInteger("id", id);
		q.setParameter("active", true);
		return (List<Product>) q.list();
	}

	@Override
	public Product get(int id) {

		return (Product) sessionFactory.getCurrentSession().get(Product.class, id);
	}

	@Override
	public boolean add(Product product) {
		try {
			sessionFactory.getCurrentSession().persist(product);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean update(Product product) {
		try {
			sessionFactory.getCurrentSession().update(product);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;

	}

	@Override
	public boolean delete(Product product) {
		try {

			product.setActive(false);
			// call the update method
			return this.update(product);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> listActiveProducts() {
		String selectActiveProducts = "FROM Product WHERE active = :active";
		return (List<Product>) sessionFactory.getCurrentSession().createQuery(selectActiveProducts)
				.setParameter("active", true).list();
	}

}
