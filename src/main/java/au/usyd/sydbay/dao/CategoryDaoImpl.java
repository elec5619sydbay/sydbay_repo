package au.usyd.sydbay.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.sydbay.domain.Category;

@Repository("categoryDao")
@Transactional
public class CategoryDaoImpl implements CategoryDao {

	@Resource
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public boolean add(Category category) {
		try {
			sessionFactory.getCurrentSession().persist(category);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Category> listAllCategory() {

		List<Category> categories;
		Query query = sessionFactory.getCurrentSession().createQuery("from Category");
		
		categories = (List<Category>) query.list();

		return categories;
	}

	@Override
	public Category get(int id) {

		return (Category) sessionFactory.getCurrentSession().get(Category.class, id);
	}

	@Override
	public boolean update(Category category) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Category getByName(Category category) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Category where name = :name");
		query.setString("name", category.getName());
		
		return (Category) query.list().get(0);
	}

}
