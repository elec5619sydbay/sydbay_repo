package au.usyd.sydbay.dao;


import java.util.List;

import au.usyd.sydbay.domain.AdminAccount;

public interface AdminDao {

	boolean add(AdminAccount adminaccount );
	List<AdminAccount> listAllAdminAccount();
	AdminAccount get(int id);
	AdminAccount GetByName(String name);

}