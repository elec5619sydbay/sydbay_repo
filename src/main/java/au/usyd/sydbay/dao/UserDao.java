package au.usyd.sydbay.dao;

import java.util.List;
import java.util.Map;

import au.usyd.sydbay.domain.User;

public interface UserDao {

	void addUser(User user);
	
	User findUserByUsername(String username);
	

	User findUserById(int id);


	User deleteUserByid(int id);

	List<User> listAllUser();


	public User getUser(String username, String password);
	
	public User getUser(String username);
	
	public boolean updateUser(User user, Map<String, String> userInfo);
	void update(User user);
	
	public List<User> findAllUser();
	
	public void deleteUser(String username);

}
