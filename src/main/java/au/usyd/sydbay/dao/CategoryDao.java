package au.usyd.sydbay.dao;


import java.util.List;

import au.usyd.sydbay.domain.Category;

public interface CategoryDao {

	boolean add(Category category);
	
	List<Category> listAllCategory();
	
	Category get(int id);
	
	Category getByName(Category category);
	
	boolean update(Category category);
	
	boolean delete(int id);
}