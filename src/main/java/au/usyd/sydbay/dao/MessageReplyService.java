package au.usyd.sydbay.dao;

import java.util.List;

import au.usyd.sydbay.domain.MessageReply;

public interface MessageReplyService {

	public List<MessageReply> searchMessageReply(int number);           //查询留言回复
	
	public void saveMessageReply(MessageReply messageReply);
	
	public void delReply(long id);
	
	public void deleteReply(long id);
	
}
