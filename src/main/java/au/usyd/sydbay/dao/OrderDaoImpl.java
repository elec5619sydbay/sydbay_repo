package au.usyd.sydbay.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.sydbay.domain.OrderItem;
import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.service.ProductService;

@Repository("orderDao")
@Transactional
public class OrderDaoImpl implements OrderDao {

	@Resource
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private ProductService pService;

	@Override
	public List<OrderItem> findAllOrderItemsByUserId(int id) {

		return (List<OrderItem>) sessionFactory.getCurrentSession().createQuery("from OrderItem where userId = :id")
				.setInteger("id", id).list();
	}
	
	@Override
	public void addOrderItem(OrderItem orderItem) {
		sessionFactory.getCurrentSession().persist(orderItem);
	}



	@Override
	public List<Product> findBiddingProducts(int id) {
		List<OrderItem> orderList = this.findAllOrderItemsByUserId(id);
		List<Product> products = new ArrayList<Product>();
		List<Integer> pIDs = new ArrayList<Integer>();
		for (int i=0; i < orderList.size(); i++) {
			OrderItem order = orderList.get(i);
			int pId = order.getProductId();
			if (!pIDs.contains(pId)) {
				if (pService.getById(pId).isActive()) {
					products.add(pService.getById(pId));
					pIDs.add(pId);
				}
			}
		}
		
		return products;
	}

}
