package au.usyd.sydbay.dao;

import java.util.List;

import au.usyd.sydbay.domain.Message;
import au.usyd.sydbay.domain.MessageReply;
import au.usyd.sydbay.vo.ListVo;

public interface MessageService {
	
	public List<ListVo> getMessage(int number);         //查询留言
	
	public void saveMessage(Message message);            //留言
	
	public void updateMessage(long id,MessageReply messageReply);
	
	public Message searchMessage(long id);
	
	public void delMessage(long id);                       //删除留言
	
}
