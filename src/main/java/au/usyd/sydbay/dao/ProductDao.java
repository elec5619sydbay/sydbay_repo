package au.usyd.sydbay.dao;


import java.util.List;

import au.usyd.sydbay.domain.Product;

public interface ProductDao {

	List<Product> listAllProduct();
	
	List<Product> listProductByCategory(int id);
	
	List<Product> listActiveProducts();
	
	List<Product> listActiveProductByCategory(int id);
	
	Product get(int id);
	
	boolean add(Product product);
	boolean update(Product product);
	boolean delete(Product product);
}