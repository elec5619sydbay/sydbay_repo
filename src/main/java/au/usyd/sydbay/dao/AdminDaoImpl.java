package au.usyd.sydbay.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.sydbay.domain.AdminAccount;
import au.usyd.sydbay.domain.User;

@Repository("AdminDao")
@Transactional
public class AdminDaoImpl implements AdminDao {

	@Resource
	@Autowired
	private SessionFactory sessionFactory;
	

	@Override
	public boolean add(AdminAccount adminaccount) {
		try{
			//add the adminaccount to database table
			sessionFactory.getCurrentSession().persist(adminaccount);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
		return false;	
		}
		
	}

	@Override
	public AdminAccount get (int id) {
		// Get admin entity by id
		return (AdminAccount) sessionFactory.getCurrentSession().get(AdminAccount.class,id);
	}

	@Override
	public List<AdminAccount> listAllAdminAccount() {
		// get list of admin entities
		List<AdminAccount> adminAccount;
		Query q= sessionFactory.getCurrentSession().createQuery("FROM AdminAccount");
		adminAccount = (List<AdminAccount>) q.list();
		return adminAccount;
	}

	@Override
	public AdminAccount GetByName(String name) {
		// Get admin by name
		Query query = sessionFactory.getCurrentSession().createQuery("from AdminAccount where name = :name");
		query.setString("name", name);
		if (query.list().size() > 0) {
			return (AdminAccount) query.list().get(0);
		} else {
			return null;
		}
	}
	
	

}
