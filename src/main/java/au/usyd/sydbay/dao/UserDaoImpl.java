/**
 * 
 */
package au.usyd.sydbay.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.sydbay.domain.User;

/**
 * @author Zara,Jason
 *
 */
@Transactional
@Repository("userDao")
public class UserDaoImpl implements UserDao {

	@Autowired
	@Resource
	SessionFactory sessionFactory;


	/*
	 * (non-Javadoc)
	 * 
	 * @see au.usyd.sydbay.dao.UserDao#addUser(au.usyd.sydbay.domain.User)
	 */
	@Override
	public void addUser(User user) {
			sessionFactory.getCurrentSession().persist(user);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see au.usyd.sydbay.dao.UserDao#findUserByUsername(java.lang.String)
	 */
	@Override
	public User findUserByUsername(String username) {
		Query query = sessionFactory.getCurrentSession().createQuery("from User where username = :name");
		query.setString("name", username);
		if (query.list().size() > 0) {
			return (User) query.list().get(0);
		} else {
			return null;
		}

	}


	@Override
	public User findUserById(int id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}
	
	
//	//Start here
//	public User getUser(String username, String password) {
//		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
//		criteria.add(Expression.like("username", username)).add(Expression.like("password", password));
//		return (User) criteria.uniqueResult();
//	}
//
//	//search by username
//	public User getUser(String username) {
//		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
//		criteria.add(Expression.like("username", username));
//		return (User) criteria.uniqueResult();
//	}
	
	public boolean updateUser(User user, Map<String, String> userInfo) {
		//todo 
		user.setPassword(userInfo.get("newPassword2"));
		
		//user.setPhone(userInfo.get(“phone”));
		user.setEmail(userInfo.get("email"));
		sessionFactory.getCurrentSession().update(user);
		return true;
	}

	

	public List<User> findAllUser(){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		List<User> users =(List<User>) criteria.list();
		return users;
	}

//	private User getUsername(){
//
//	}

	public void deleteUser(String username) {
		sessionFactory.getCurrentSession().delete(getUser(username));
	}

@Override
public User getUser(String username, String password) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public User getUser(String username) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void update(User user) {
	sessionFactory.getCurrentSession().update(user);
	
}



	@Override
	public User deleteUserByid(int id) {
		User a = (User)sessionFactory.getCurrentSession().get(User.class,id);
		sessionFactory.getCurrentSession().delete(a);
		
		return null;
	}

	@Override
	public List<User> listAllUser() {
		// TODO Auto-generated method stub
		List<User> users;
		Query query = sessionFactory.getCurrentSession().createQuery("from user");
		users = (List<User>) query.list();
		return users;
	}

}
