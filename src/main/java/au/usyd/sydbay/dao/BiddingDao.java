package au.usyd.sydbay.dao;

import java.util.List;

import au.usyd.sydbay.domain.BiddingItem;

public interface BiddingDao {

	List<BiddingItem> findAllBiddingItemByProductId(int id);
	
	void add(BiddingItem biddingItem);
}
