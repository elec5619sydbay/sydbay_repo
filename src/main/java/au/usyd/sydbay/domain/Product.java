package au.usyd.sydbay.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String code;

	@Column
	@NotBlank(message = "Please enter the product tile!")
	private String name;

	@Column
	@NotBlank(message = "Please enter the product description!")
	private String description;

	@Column
	@NotBlank(message = "Please select a begining date!")
	private String startDate;

	@Column
	@Min(value = 1, message = "Duration should longer than 1 day and less than 10 days!")
	@Max(value = 10, message = "Duration should longer than 1 day and less than 10 days!")
	private int duration;

	@Column
	@Min(value = 1, message = "Initial price should be over $1")
	private float initPrice;

	@Column
	@Min(value = 1, message = "The minimum rising price every bids should be over $1")
	private int addAmount;

	@Column
	@Min(value = 1, message = "Enter an Buy-it-now price!")
	private float fixedPrice;
	@Column
	private float currentPrice;

	/**
	 * @return the currentPrice
	 */
	public float getCurrentPrice() {
		return currentPrice;
	}

	/**
	 * @param currentPrice the currentPrice to set
	 */
	public void setCurrentPrice(float currentPrice) {
		this.currentPrice = currentPrice;
	}

	@Column
	@Min(value = 0, message = "Please select the condition of the product!")
	@Max(value = 2, message = "Please select the condition of the product!")
	private int productCondition;

	@Column
	private int status;
	
	@Column(name = "is_active", columnDefinition = "TINYINT", length = 1)
	private boolean active;

	@Column
	private int categoryId;

	@Column
	private int supplierId;
	private int purchases;
	private int views;

	@Transient
	private MultipartFile file;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	// default constructor
	public Product() {

		this.code = "PRD" + UUID.randomUUID().toString().substring(26).toUpperCase();

	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 * @throws ParseException
	 */
	public void setStartDate(String startDate) throws Exception {
		Date today = new Date();
		String str = new SimpleDateFormat("yyyy-MM-dd").format(today);
		if (str.equals(startDate)) {
			// submit today
			this.startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(today);
			;
		} else {
			// submit another day
			this.startDate = startDate + " 00:00";
		}

	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return the initPrice
	 */
	public float getInitPrice() {
		return initPrice;
	}

	/**
	 * @param initPrice
	 *            the initPrice to set
	 */
	public void setInitPrice(float initPrice) {
		this.initPrice = initPrice;
	}

	/**
	 * @return the addAmount
	 */
	public int getAddAmount() {
		return addAmount;
	}

	/**
	 * @param addAmount
	 *            the addAmount to set
	 */
	public void setAddAmount(int addAmount) {
		this.addAmount = addAmount;
	}

	/**
	 * @return the fixedPrice
	 */
	public float getFixedPrice() {
		return fixedPrice;
	}

	/**
	 * @param fixedPrice
	 *            the fixedPrice to set
	 */
	public void setFixedPrice(float fixedPrice) {
		this.fixedPrice = fixedPrice;
	}

	/**
	 * @return the productCondition
	 */
	public int getProductCondition() {
		return productCondition;
	}

	/**
	 * @param productCondition
	 *            the productCondition to set
	 */
	public void setProductCondition(int productCondition) {
		this.productCondition = productCondition;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public int getPurchases() {
		return purchases;
	}

	public void setPurchases(int purchases) {
		this.purchases = purchases;
	}

	public int getViews() {
		return views;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", active=" + active + ", code=" + code + ", name=" + name + ", description=" + description
				+ ", startDate=" + startDate + ", duration=" + duration + ", initPrice=" + initPrice + ", addAmount="
				+ addAmount + ", fixedPrice=" + fixedPrice + ", productCondition=" + productCondition + ", status="
				+ status + ", categoryId=" + categoryId + ", category=" + categoryId + ", supplierId=" + supplierId
				+ ", purchases=" + purchases + ", views=" + views + "]";
	}

}
