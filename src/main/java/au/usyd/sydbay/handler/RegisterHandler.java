package au.usyd.sydbay.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.stereotype.Component;

import au.usyd.sydbay.dao.UserDao;
import au.usyd.sydbay.domain.User;
import au.usyd.sydbay.model.RegisterModel;

@Component
public class RegisterHandler {

	@Autowired
	UserDao userDao;

	public RegisterModel init() {
		return new RegisterModel();
	}

	public String saveUser(User user) {
		String transitionValue = "success";

		System.out.println("user:" + user.toString());
		userDao.addUser(user);

		return transitionValue;
	}

	public String validateUser(User user, MessageContext error) {
		String transitionValue = "success";

		// checking if password equals comfirm password
		if (!(user.getPassword().equals(user.getConfirmPassword()))) {

			error.addMessage(new MessageBuilder().error().source("comfirmPassword")
					.defaultText("Password does not match the confirm password!").build());
			transitionValue = "failure";
		}

		if (userDao.findUserByUsername(user.getUsername()) != null) {
			transitionValue = "failure";
			error.addMessage(new MessageBuilder().error().source("username")
					.defaultText("Username is already used!").build());
		}

		return transitionValue;
	}
}
