package au.usyd.sydbay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.sydbay.dao.CategoryDao;
import au.usyd.sydbay.domain.Category;

@Service(value = "categoryService")
public class CategoryService {

	@Autowired
	private	CategoryDao categoryDao;
	
	public void addCategory(Category category){
		//looking for the category in database;
		if(categoryDao.getByName(category) != null){
			//if it doesn't exist, then add
			categoryDao.add(category);
		}
		
	}
	
	public Category getCategoryById(int id){
		return categoryDao.get(id);
	}
	
	public List<Category> getAllCategories(){
		return categoryDao.listAllCategory();
	}
}
