package au.usyd.sydbay.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.sydbay.dao.OrderDao;
import au.usyd.sydbay.domain.OrderItem;
import au.usyd.sydbay.domain.Product;

@Service(value="orderService")
public class OrderService {

	@Autowired
	OrderDao orderDao;
	
	public List<OrderItem> findOrderByUserId(int id){
		return orderDao.findAllOrderItemsByUserId(id);
	}
	
	public void addOrderItem(OrderItem orderItem){
		orderDao.addOrderItem(orderItem);
	}
	
	public List<Product> findBiddingProducts(int id){
		return orderDao.findBiddingProducts(id);
	}
}
