package au.usyd.sydbay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.sydbay.dao.AdminDao;
import au.usyd.sydbay.domain.AdminAccount;

@Service(value = "AdminService")
public class AdminServices {
	
	@Autowired
	private	AdminDao adminAcc;
	
	public void addAdmin(AdminAccount addmin){
		
		boolean a = adminAcc.add(addmin);
	}

	public AdminAccount getById(int id){
		return adminAcc.get(id);
	}
	
	public AdminAccount getAdById(String a){
		return adminAcc.GetByName(a);
	}
	
}
