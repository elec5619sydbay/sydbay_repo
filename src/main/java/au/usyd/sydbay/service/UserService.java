package au.usyd.sydbay.service;

import java.util.List;

import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.sydbay.dao.UserDao;
import au.usyd.sydbay.domain.Product;
import au.usyd.sydbay.domain.User;

@Service(value="userService")
public class UserService{

	@Autowired
	UserDao userDao;
	
	public String addUser(User user){
		
		if(userDao.findUserByUsername(user.getUsername()) != null){
			return "Username already exist!";
		}else{
			userDao.addUser(user);
			return "User successfully added";
		}
		 
	}

	public void DeleteUser(int id){
		userDao.deleteUserByid(id);
	}
	
	public User findById(int id){
		return userDao.findUserById(id);
	}
	
	public User findByUsername(String name){
		return userDao.findUserByUsername(name);

	}
	
	public List<User> getAllUsers(){
		return userDao.listAllUser();
	}
	
	//Start here
	public User getUser(String username) {
		return userDao.getUser(username);
	}
	
	public boolean updateUser(User user, Map<String, String> userInfo) {
		return userDao.updateUser(user, userInfo);
	}
	
	public List<User> findAllUser() {
		return userDao.findAllUser();
	}
	
	public void deleteUser(String username) {
		userDao.deleteUser(username);
	}
	
	public void update(User user){
		userDao.update(user);
	}
	

}
