package au.usyd.sydbay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.sydbay.dao.BiddingDao;
import au.usyd.sydbay.domain.BiddingItem;

@Service(value="biddingService")
public class BiddingService {
	
	@Autowired
	BiddingDao biddingDao;
	
	public List<BiddingItem> getAllBiddingItemByProduct(int id){
		
		return biddingDao.findAllBiddingItemByProductId(id);
	}
	
	public void addBiddingItem(BiddingItem biddingItem){
		biddingDao.add(biddingItem);
	}
}
