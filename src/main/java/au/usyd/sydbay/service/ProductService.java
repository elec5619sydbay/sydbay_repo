package au.usyd.sydbay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.sydbay.dao.ProductDao;
import au.usyd.sydbay.domain.Product;

@Service(value = "productService")
public class ProductService {
	
	@Autowired
	private	ProductDao productDao;
	
	public void addProduct(Product product){
		//TODO how to check whether a product exist? By name?
		productDao.add(product);
	}

	public Product getById(int id){
		return productDao.get(id);
	}
	
	public List<Product> getAllProducts(){
		return productDao.listAllProduct();
	}
	
	public List<Product> getProductsByCategory(int id){
		return productDao.listProductByCategory(id);
	}
	
	public List<Product> getActiveProductsByCategory(int id){
		return productDao.listActiveProductByCategory(id);
	}
	
	public List<Product> listAllActiveProducts() {
		return productDao.listActiveProducts();
	}
	
	public void updateProduct(Product product){
		productDao.update(product);
	}

}
