package au.usyd.sydbay.util;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

public class FileUploadUtility {
	
//	private static final String ABS_PATH = "/Users/ProLee/Desktop/sydbay_repo/src/main/webapp/resources/images/";
	private static final String ABS_PATH = "E:\\Master\\Semester3\\ELEC5619 Object Oriented Application Frameworks\\sydbay\\src\\main\\webapp\\resources\\images\\";
	private static String REAL_PATH = "";
	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadUtility.class);
	public static void uploadFile(HttpServletRequest request, MultipartFile file, String code) {
		
		// get real path
		REAL_PATH = request.getSession().getServletContext().getRealPath("/resources/images/");
		
		logger.info(REAL_PATH);
		
		// check the directory exists or create one
		if(!new File(ABS_PATH).exists()) {
			new File(ABS_PATH).mkdirs();
		}
		
		if(!new File(REAL_PATH).exists()) {
			new File(REAL_PATH).mkdirs();
		}
		
		try {
			// server upload
			file.transferTo(new File(REAL_PATH + code + ".jpg"));
			// project directory upload
			file.transferTo(new File(ABS_PATH + code + ".jpg"));
			
		} catch (IOException e) {

		}
	}
	
}
