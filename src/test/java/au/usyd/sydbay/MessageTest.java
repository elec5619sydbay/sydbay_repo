package au.usyd.sydbay;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.usyd.sydbay.domain.Message;
import au.usyd.sydbay.domain.MessageReply;
import au.usyd.sydbay.dao.MessageService;
import org.hibernate.Session;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MessageTest {

	@Test
	public void test() {
		Message message = new Message();
		List<MessageReply> reply = new ArrayList<MessageReply>();
		MessageReply messageReply1 = new MessageReply();
		MessageReply messageReply2 = new MessageReply();
		
		message.setUsername("haha2");
		message.setContent("Today is sunday.");
		Date date = new Date();
		message.setTime(date);
		
		messageReply1.setContent("Today");
		messageReply1.setTime(date);
		messageReply1.setUsername("hehe");
		reply.add(messageReply1);
		
		messageReply2.setContent("momo");
		messageReply2.setTime(date);
		messageReply2.setUsername("hehe");
		reply.add(messageReply2);
		message.setReply(reply);

		ApplicationContext context = new ClassPathXmlApplicationContext("/au/usyd/sydbay/config/applicationContext.xml");
		MessageService messageService = (MessageService) context.getBean("messageService");
		messageService.saveMessage(message);
	}
	
	@Test
	public void test1(){
		ApplicationContext context = new ClassPathXmlApplicationContext("/au/usyd/sydbay/config/applicationContext.xml");
		MessageService messageService = (MessageService) context.getBean("messageService");
		
		Date date = new Date();
		MessageReply messageReply3 = new MessageReply();
		messageReply3.setContent("sun");
		messageReply3.setTime(date);
		messageReply3.setUsername("hehe22");
		long a = 1;
		messageService.updateMessage(a, messageReply3);
		
	}
	
	@Test 
	public void test2(){
		ApplicationContext context = new ClassPathXmlApplicationContext("/au/usyd/sydbay/config/applicationContext.xml");
		MessageService messageService = (MessageService) context.getBean("messageService");
		
		Message m = messageService.searchMessage(1);
		List<MessageReply> re = m.getReply();
		
		for(MessageReply mr : re){
			System.out.println(mr.getContent());
		}
	}

}