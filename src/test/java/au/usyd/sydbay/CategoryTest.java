package au.usyd.sydbay;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

import au.usyd.sydbay.controller.PageController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml" })
public class CategoryTest {

	@Autowired
	ApplicationContext applicationContext;

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private HandlerAdapter handlerAdapter;

	@Autowired
	private PageController pageController;

	@Before
	public void setUp() {
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		handlerAdapter = new AnnotationMethodHandlerAdapter();
	}

	@Test
	public void testTitle() throws Exception {

		request.setRequestURI("/home");
		request.setMethod("GET");
		final ModelAndView mav = handlerAdapter.handle(request, response, pageController);

		assertEquals("Home", mav.getModel().get("title"));
		assertEquals(true, mav.getModel().get("userClickHome"));

	}
	/*
	 * private static AnnotationConfigApplicationContext context;
	 * 
	 * private static CategoryDao categoryDao;
	 * 
	 * private Category category;
	 * 
	 * @BeforeClass public static void init(){
	 * 
	 * context = new AnnotationConfigApplicationContext();
	 * context.scan("au.usyd.sydbay"); context.refresh();
	 * 
	 * categoryDao = (CategoryDao) context.getBean("categoryDao"); }
	 * 
	 * @Test public void testAddCategory(){ category = new Category();
	 * category.setName("Man");
	 * 
	 * assertEquals("successfull added a category inside the table", true,
	 * categoryDao.add(category)); }
	 */}
