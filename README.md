# Syd-Bay

Syd-Bay is a website which to help students at the University of Sydney make full use of that stuff. Here comes our idea. People can sell their stuff on our website called “SydBay” to those who need them. Not only graduates but also students undergraduate can have an account on “SydBay” and sell kinds of stuff they do not need anymore.

## Demo Video

Youtube:
https://www.youtube.com/watch?v=oOhBDEG1wgg

Youku (For Mainland China users):
http://v.youku.com/v_show/id_XMzQ0OTIyODkyNA==.html?spm=a2h3j.8428770.3416059.1

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This app requires:
Using Eclipse or any other similar Java IDE to run.
An installation of MySQL, running locally or available on the network.
An installation of Browser.

###Dependencies

This app requires:

* An installation of [MySQL](https://www.mysql.com/), running locally or available on the network.
* An installation of [Eclipse](https://www.eclipse.org/downloads/) or [STS](https://spring.io/tools).
* An installation of [Tomcat](http://tomcat.apache.org).

###Configuration

Configuration files are found in `src/main/resources/`

* `database.properties` configures MySQL, the database
    * **jdbc.username** The name of the host machine running MySQL.
    * **jdbc.password** The password of the MySQL database connection.

###Running

First, import the project file in to Eclipse,

Right click the project title and select `Maven -> Update Project`,

Deploy the built path of the project again,

Right click the project then run the project on Tomcat server (Version 8.0 recommanded)

You should then be able to see the forum in your browser at [localhost:8080/sydbay/](http://localhost:8080/sydbay). 

###Design guidelines

This app is designed follow Spring MVC framework.

API is supported by Jackson.JAR for JSON data transforming

Front end has constructered base on jQuery, Bootstrap, DataTable.js, BootBox.js, etc. 


###Server Side (Java)

The Java code's job is to provide a nice, clean, well-documented REST API.

* [Spring MVC](http://spring.io/guides/gs/rest-service) is used to greatly reduce the amount of boilerplate. 


MVC pattern: domain -> DAO -> server -> controller -> util, validator, test  


###Contact us
If you have any advises, welcome to cantact us: jasonluo3329@gmail.com or mustljy@gmail.com
    

