-- adding categories
INSERT INTO category (name) VALUES ("Furniture");
INSERT INTO category (name) VALUES ("Sports");
INSERT INTO category (name) VALUES ("Books");
INSERT INTO category (name) VALUES ("Music");
INSERT INTO category (name) VALUES ("Beauty");
INSERT INTO category (name) VALUES ("Electronics");
INSERT INTO category (name) VALUES ("Clothes");

-- adding products
insert into product (name,categoryId,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'01',5,150,0,5,0);
insert into product (name,categoryId,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'02',5,150,0,5,0);
insert into product (name,categoryId,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'03',5,150,0,5,0);
insert into product (name,categoryId,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'04',5,150,0,5,0);
insert into product (name,categoryId,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'05',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'06',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'07',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'08',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'09',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'10',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'11',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'12',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'13',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'14',5,150,0,5,0);
insert into product (name,category_id,initPrice,image,addAmount,fixedPrice,productCondition,duration,status) values ('Makeup Bag',5,60,'15',5,150,0,5,0);